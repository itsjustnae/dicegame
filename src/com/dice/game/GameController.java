package com.dice.game;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.io.File;

public class GameController{

    @FXML
    private Label score;
    @FXML
    private Label score2;
    @FXML
    private Label result;
    @FXML
    private Label result2;
    @FXML
    private Button rollBtn;


    @FXML protected  void  handleSubmitButtonAction(ActionEvent event) {
        int value = (int) (Math.random() * 25) + 1;
        int value2 = (int) (Math.random() * 25) + 1;
        score.setText(value + "");
        score2.setText(value2 + "");

        Media media = new Media(new File("rolldice.mp3").toURI().toString());
        MediaPlayer player = new MediaPlayer(media);
        player.play();


        if(value > value2){
            result.setText("Player: 1 Wins!");
        }else if(value2 > value){
            result.setText("Player: 2 Wins!");
        }else if (value == value2 || value2 == value){
            result.setText("It's a Tie!!!");
        }

    }


}







